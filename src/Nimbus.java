
import bot.Bot;
import bot.RunescapeStub;
import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Insets;

import javax.swing.JFrame;

import bot.listener.BotEventListener;
import bot.listener.ChatListener;
import bot.model.ChatMessage;
import bot.model.LocalNPC;
import java.util.Random;
//this class extracts information from the client,
//and handles typing it out
public class Nimbus extends Bot {

    public static void main(String[] args) {
        new Nimbus();
    }
    public Client client;
    public Applet gameApplet;
    public JFrame add;
    private LocalNPC[] npcs;

    @SuppressWarnings("serial")
    public Nimbus() {
        client = new Client() {
      
            public void processedClientUpdates() {
                setChanged();
                notifyObservers(client);
            }
        };

        gameApplet = ((Applet) client);
        gameApplet.setStub(new RunescapeStub());
        add = new JFrame("NimbusBot - Silabsoft IAMNULL");
        add.add(gameApplet, BorderLayout.CENTER);
        Insets in = add.getInsets();
        add.setSize(in.right + in.left + 765 + 5, in.bottom + in.top + 503 + 30);
        add.setVisible(true);
        add.setResizable(false);
        gameApplet.start();
        gameApplet.init();
        gameApplet.addKeyListener(new Keyboard(this));
        this.setBot(client);
    }

    @Override
    public void writeChat(String s) {
        client.RD.I(4);
        client.RD.Z(0);
        int i4 = client.RD.Z;
        client.RD.A(0);
        client.RD.A(0);
        client.equalsIgnoreCase.Z = 0;
        AC.I(s, client.equalsIgnoreCase);
        client.RD.I(0, client.equalsIgnoreCase.I, client.equalsIgnoreCase.Z);
        client.RD.F(client.RD.Z - i4);
    }
}
