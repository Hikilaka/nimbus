package bot.util;
import java.io.File;
import java.io.FileFilter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ClassAggregator implements Iterable<Class> {

    private File classDir = null;
    private boolean recursiveSearch = false;
    private Class filterClass = null;
    private static FileFilter filter = new ClassFilter();
    private Map<String, Class> classFiles = new HashMap<String, Class>();

    public ClassAggregator(File f, boolean recursive) {
        this(f, null, recursive);
    }

    public ClassAggregator(File f, Class c, boolean recursive) {
        setDir(f);
        setRecursiveSearch(recursive);
        setFilterClass(c);
        loadFiles();
    }

    public Class getFilterClass() {
        return filterClass;
    }

    public void setFilterClass(Class c) {
        filterClass = c;
    }

    public File getDir() {
        return classDir;
    }

    public void setDir(File f) {
        if (f.isDirectory()) {
            classDir = f;
        }
    }

    public boolean isRecursiveSearch() {
        return recursiveSearch;
    }

    public void setRecursiveSearch(boolean b) {
        recursiveSearch = b;
    }

    public void loadFiles() {
        loadFiles(getDir());
    }

    private void loadFiles(File dir) {
        try {
            URLClassLoader ucl = new URLClassLoader(new URL[]{dir.toURL()});
            for (File f : dir.listFiles(filter)) {
                if (f.isDirectory()) {
                    if (isRecursiveSearch()) {
                        loadFiles(dir);
                    }
                } else {
                    String className = f.getName().substring(0, f.getName().indexOf("."));
                    Class c = ucl.loadClass(className);
                    if (filterClass == null
                            || (filterClass != null && filterClass.isAssignableFrom(c))) {
                        classFiles.put(className, c);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Iterator<Class> iterator() {
        return classFiles.values().iterator();
    }
}

class ClassFilter implements FileFilter {

    public boolean accept(File f) {
        String name = f.getName();
        return f.isDirectory() || (name.endsWith(".class") && name.indexOf("$") == -1);
    }
}
