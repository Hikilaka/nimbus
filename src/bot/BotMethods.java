/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot;

import bot.model.LocalNPC;
import bot.model.Skill;
import bot.slave.hook.NPC;

/**
 *
 * @author Silabsoft
 */
public class BotMethods {

    private final Bot bot;
    private PacketForge pf;

    public BotMethods(Bot b) {
        bot = b;
        pf = new PacketForge(bot.getClient().getStream());
    }

    private int findNpc(String s, boolean checkInteraction) {
        if (bot.NpcsInRange() == null) {
            return -1;
        }
        for (LocalNPC n : bot.NpcsInRange()) {
            NPC npc = n.getNPC();
            if (npc == null || !npc.isDefinitionSet()) {
                continue;
            }
            if (npc.getNPCDef().getName().equalsIgnoreCase(s)) {
                if (checkInteraction && npc.getInteractingEntity() == -1 && npc.getAnimation() == -1 || !checkInteraction) {
                    return n.getIndex();
                }

            }

        }
        return -1;
    }

    public boolean attackNpc(String name) {
        int index = findNpc(name, true);
        if (index == -1) {
            return false;
        }
        pf.attackNpc(index);
        //should insert some check to see if you are attacking the npc here
        return true;
    }

    public boolean npcAction(String name, int id) {
        int index = findNpc(name, false);
        if (index == -1) {
            return false;
        }
        pf.npcAction(index, id);
        return true;
    }

    public int getExperienceForSkill(Skill s) {
        return bot.getClient().getSkillExperience()[s.getId()];
    }

    public int getLevelForSkill(Skill s) {
        return bot.getClient().getSkillLevels()[s.getId()];
    }

    public int getStatForSkill(Skill s) {
        return bot.getClient().getSkillStats()[s.getId()];
    }

    public void useItem(int frame, int slot, int itemId) {
        pf.useItem(frame, slot, itemId);
    }
}
