/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot;

import bot.clientobservers.ChatObserver;
import bot.clientobservers.ClientObserver;
import bot.clientobservers.NpcObserver;
import bot.clientobservers.SkillObserver;
import bot.event.EventManager;
import bot.listener.ChatListener;
import bot.model.LocalNPC;
import bot.slave.hook.Client;
import java.util.Observable;

/**
 *
 * @author Silabsoft
 */
public class Bot extends Observable {

    private LocalNPC[] npcs = null;
    private Client client;
    private BotMethods botMethods;
    protected ChatObserver chatObserver;
    private EventManager eventManager;
    private NpcObserver npcOverserver;
    private SkillObserver skillObserver;

    public void setBot(Client client) {
        this.client = client;
        this.botMethods = new BotMethods(this);
        this.npcOverserver = new NpcObserver();
        this.addObserver(npcOverserver);
        this.chatObserver = new ChatObserver();
        this.addObserver(chatObserver);
        this.eventManager = new EventManager(this);
        this.skillObserver = new SkillObserver();
        this.addObserver(skillObserver);
        
    }

    public LocalNPC[] NpcsInRange() {
        return npcs;
    }

    public void setNPCsInRange(LocalNPC[] npc) {
        this.npcs = npc;
    }

    public BotMethods getBotMethods() {
        return botMethods;
    }

    public Client getClient() {
        return client;
    }

    public static void log(String message) {
        System.out.println(message);
    }

    public ChatObserver getChatObserver() {
        return chatObserver;
    }
    
    //this is temporary until I hook the stuff needed to put it in the interface
    public void writeChat(String s){
        
    }
    public void npcAction1(int index){
        
    }

    public NpcObserver getNpcObserver() {
        return npcOverserver;
    }
}
