package bot.slave.hook;

/**
 * 
 * @author Hikilaka
 *
 */
public interface ObjectDef {
	
	public String getName();
	
	public String getDescription();

}