package bot.slave.hook;

public interface Mobile extends Entity {

    public boolean isVisible();

    public int getAnimation();

    public int getCurrentHealth();

    public int getMaxHealth();

    public int getGraphic();

    public int getInteractingEntity();

    public String getForcedHeadChat();

    public int getFaceX();

    public int getFaceY();
}
