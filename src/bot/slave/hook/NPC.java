package bot.slave.hook;

public interface NPC extends Mobile {

    public NPCDef getNPCDef();

    public boolean isDefinitionSet();

 
}
