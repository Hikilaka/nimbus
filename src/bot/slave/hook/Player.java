package bot.slave.hook;

public interface Player extends Mobile {

    public int getGender();

    public String getName();

    public NPCDef getNPCMaskDef(); //i can't ever think of when this is used but if a player is masked as an NPC this has a value greater than 0

    public int getCombatLevel();

    public int getHeadIcon();

    public int getSkill(); //not sure on this one need to investigate what its used for.

}
