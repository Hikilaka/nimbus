package bot.slave.hook;

public interface Client {

    public Player getMyPlayer();

    public NPC[] getNPCs();

    public Player[] getPlayers();

    public Deque[][][] getGroundArray();
    //called everytime the client is updated will be used for the bot similiar to aryan

    public void processedClientUpdates();

    public String[] getChatMessages();

    public String[] getChatNames();

    public int[] getChatTypes();

    public int[] getSessionNpcList();

    public int getSessionNpcCount();

    public Stream getStream();

    public int getBaseX();

    public int getBaseY();
    public int[] getSkillExperience();
public int[] getSkillStats();
public int[] getSkillLevels();
}
