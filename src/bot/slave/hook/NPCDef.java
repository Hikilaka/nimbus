package bot.slave.hook;

public interface NPCDef {
	
    public String getName();
    public boolean isClickable();
}
