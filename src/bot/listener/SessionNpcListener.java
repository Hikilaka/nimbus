/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.listener;

import bot.slave.hook.NPC;

/**
 *
 * @author Silabsoft
 */
public interface SessionNpcListener {
    public void onAnimation(NPC n,int animation);
    public void onGraphic(NPC n,int graphic);
    public void onEntityInteraction(NPC n,int entityId);
    public void onForcedHeadChat(NPC n, String message);
    public void onFaceTile(NPC n,int x,int y);
    public void onHealthChange(NPC n, int current, int max);
}
