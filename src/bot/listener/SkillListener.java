/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.listener;

import bot.model.Skill;

/**
 *
 * @author Silabsoft
 */
public interface SkillListener {
    public void onSkillLevelGained(Skill s, int levelDifference);
    public void onSkillStatChange(Skill s, int difference);
    public void onSkillExperinceGained(Skill s, int gained);
}
