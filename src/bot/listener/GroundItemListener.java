/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.listener;

/**
 *
 * @author Silabsoft
 */
public interface GroundItemListener {
    public void onGroundItemAdded(int x,int y);
    public void onGroundItemRemoved(int x, int y);
}
