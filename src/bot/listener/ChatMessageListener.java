/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.listener;

import bot.model.ChatMessage;

/**
 *
 * @author Silabsoft
 */
public interface ChatMessageListener extends BotEventListener{
    public void onChatMessage(ChatMessage m);
}
