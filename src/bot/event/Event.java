/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.event;

/**
 *
 * @author Silabsoft
 */
public abstract class Event {

    public abstract void run();
    protected boolean doRun;

    public boolean pauseScriptOnRun() {
        return false;
    }

    public boolean requestRun() {
        return true;
    }

    public String getName() {
        return null;
    }

    public void setRun(boolean b) {
        doRun = b;
    }
}
