/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.event;

import bot.Bot;
import bot.Constant;
import bot.event.Event;
import bot.util.ClassAggregator;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Silabsoft
 */
public class EventManager implements Runnable {

    public final static HashMap<String, Event> events = new HashMap<String, Event>();
    private final Bot bot;
    private static EventManager manager;
    private boolean running;

    public EventManager(Bot bot) {
        this.bot = bot;

        new Thread(this).start();
    }

    @Override
    public void run() {
        this.manager = this;
        this.running = true;
        loadEvents(Constant.EVENT_FOLDER);
        bot.log("EventManager Started");
        while (running) {
            for (Event e : events.values()) {
                e.run();
            }
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
            }
        }
    }

    public static EventManager getEventManager() {
        return manager;
    }

    public Bot getBot() {
        return bot;
    }

    private void loadEvents(String folder) {
        ClassAggregator ca = new ClassAggregator(new File(folder), Event.class, false);
        for (Class c : ca) {
            addEventClass(c);
        }
    }

    public void addEventClass(Class c) {
        try {
            bot.log("Loaded Event: " + c.getCanonicalName());
            Event e = (Event) c.newInstance();
            events.put(e.getName(), e);

        } catch (InstantiationException ex) {
            Logger.getLogger(EventManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(EventManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Event getEvent(String thiever) {
      return  events.get(thiever);
    }
}
