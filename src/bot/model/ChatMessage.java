/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.model;

/**
 *
 * @author Silabsoft
 */
public class ChatMessage {
    private final int type;
    private final String name;
    private final String message;
    public ChatMessage(int type,String name,String message){
        this.type = type;
        this.name = name;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    @Override
    public String toString() {
        return "ChatMessage{" + "type=" + type + ", name=" + name + ", message=" + message + '}';
    }
    
}
