/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.model;

import bot.slave.hook.NPC;

/**
 *
 * @author Silabsoft
 */
public class LocalNPC {

    private final int index;
    private final NPC npc;

    public LocalNPC(int index, NPC npc) {
        this.index = index;
        this.npc = npc;
    }

    public int getIndex() {
        return index;
    }

    public NPC getNPC(){
        return npc;
    }
}
