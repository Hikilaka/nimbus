package bot;

import java.applet.AppletContext;
import java.applet.AppletStub;
import java.net.URL;
import java.util.Properties;

/**
 * Created by Oliver.
 */
public class RunescapeStub implements AppletStub {

    Properties parameters = new Properties();
    URL documentBase = null;
    URL codeBase = null;


    public boolean isActive() {
        return true;
    }

    public URL getDocumentBase() {
        return documentBase;
    }

    public URL getCodeBase() {
        return codeBase;
    }

    public String getParameter(String name) {
        return null;
    }

    public AppletContext getAppletContext() {
        return null;
    }

    public void appletResize(int width, int height) {

    }

 
}
