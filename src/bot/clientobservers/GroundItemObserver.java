/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.clientobservers;

import bot.listener.GroundItemListener;
import bot.slave.hook.Client;
import bot.slave.hook.Deque;
import java.util.Iterator;
import java.util.Observable;

/**
 *
 * @author Silabsoft
 */
public class GroundItemObserver extends ClientObserver {

    private int groundArray[][][] = new int[4][104][104];
    
    @Override
    public void update(Observable o, Object o1) {
        Client c = (Client) o1;
       /* Deque d[][][] = c.getGroundArray();
        for (int p = 0; p < 4; p++) {
            for (int x = 0; x < 104; x++) {
                for (int y = 0; y < 104; y++) {
                    if (d[p][x][y] == null && groundArray[p][x][y] != 0) {

                        groundArray[p][x][y] = 0;
                        fireGroundItemRemoved(x, y);
                    } else if (d[p][x][y] != null && groundArray[p][x][y] == 0) {

                        groundArray[p][x][y] = createUID(x, y);
                        fireGroundItemAdded(x, y);
                    }
                }
            }
        }*/
    }

    public int createUID(int x, int y) {
        return x + (y << 7) + 0x60000000;
    }

  
    public void fireGroundItemAdded(int x, int y) {
        for (Iterator it = this.getListeners().iterator(); it.hasNext();) {
            GroundItemListener l = (GroundItemListener) it.next();
            l.onGroundItemAdded(x, y);
        }
    }


    public void fireGroundItemRemoved(int x, int y) {
        for (Iterator it = this.getListeners().iterator(); it.hasNext();) {
            GroundItemListener l = (GroundItemListener) it.next();
            l.onGroundItemRemoved(x, y);
        }
    }
}
