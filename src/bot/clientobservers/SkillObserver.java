/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.clientobservers;

import bot.Constant;
import bot.listener.SkillListener;
import bot.model.Skill;
import bot.slave.hook.Client;
import java.util.Iterator;
import java.util.Observable;

/**
 *
 * @author Silabsoft
 */
public class SkillObserver extends ClientObserver {

    private final int[] lastSkillExperience;
    private final int[] lastSkillLevel;
    private final int[] lastSkillStat;
    private final Skill[] skills;

    public SkillObserver() {
        lastSkillExperience = new int[Constant.SKILL_COUNT];
        lastSkillLevel = new int[Constant.SKILL_COUNT];
        lastSkillStat = new int[Constant.SKILL_COUNT];
        skills = Skill.values();
    }

    public void update(Observable o, Object o1) {
        Client c = (Client) o1;
        int[] nse = c.getSkillExperience();
        int[] nsl = c.getSkillLevels();
        int[] nss = c.getSkillStats();
        
        for(int i = 0; i < Constant.SKILL_COUNT; i++){
            int eg = nse[i] -lastSkillExperience[i];
            int lg = nsl[i] - lastSkillLevel[i];
            int s = nss[i] - lastSkillStat[i];
            if(eg > 0){
                fireExperienceChanged(skills[i],eg );
            }
            if(lg > 0){
                fireLevelGained(skills[i],lg);
            }
            fireStatChanged(skills[i],s);
        }
    }

    private void fireExperienceChanged(Skill skill, int eg) {
         for (Iterator it = this.getListeners().iterator(); it.hasNext();) {
            SkillListener l = (SkillListener) it.next();
            l.onSkillExperinceGained(skill, eg);
         }
    }

    private void fireLevelGained(Skill skill, int lg) {
         for (Iterator it = this.getListeners().iterator(); it.hasNext();) {
            SkillListener l = (SkillListener) it.next();
            l.onSkillLevelGained(skill, lg);
         }
    }

    private void fireStatChanged(Skill skill, int s) {
       for (Iterator it = this.getListeners().iterator(); it.hasNext();) {
            SkillListener l = (SkillListener) it.next();
            l.onSkillStatChange(skill, s);
       }
    }


}
