package bot.clientobservers;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
import bot.Bot;
import bot.clientobservers.ClientObserver;
import bot.listener.SessionNpcListener;
import bot.slave.hook.Client;
import bot.slave.hook.NPC;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import bot.model.LocalNPC;

/**
 *
 * @author Silabsoft
 */
public class NpcObserver extends ClientObserver {

    ArrayList<LocalNPC> inRange = new ArrayList<LocalNPC>();

    @Override
    public void update(Observable o, Object o1) {
        Client client = (Client) o1;
        NPC[] sn = client.getNPCs();
        int[] list = client.getSessionNpcList();
        int count = client.getSessionNpcCount();

        for (int i = 0; i < count; i++) {
            inRange.add(new LocalNPC(list[i], sn[list[i]]));

        }
        LocalNPC[] npcs = new LocalNPC[inRange.size()];
        npcs = inRange.toArray(npcs);
        ((Bot) o).setNPCsInRange(npcs);
        inRange.clear();

    }

    public void fireNpcAnimation(NPC n, int animation) {
        for (Iterator it = this.getListeners().iterator(); it.hasNext();) {
            SessionNpcListener l = (SessionNpcListener) it.next();
            l.onAnimation(n, animation);
        }
    }

    public void fireNpcGraphic(NPC n, int graphic) {
        for (Iterator it = this.getListeners().iterator(); it.hasNext();) {
            SessionNpcListener l = (SessionNpcListener) it.next();
            l.onGraphic(n, graphic);
        }
    }

    public void fireNpcInteraction(NPC n, int entityId) {
        for (Iterator it = this.getListeners().iterator(); it.hasNext();) {
            SessionNpcListener l = (SessionNpcListener) it.next();
            l.onEntityInteraction(n, entityId);
        }
    }

    public void fireForcedHeadChat(NPC n, String message) {
        for (Iterator it = this.getListeners().iterator(); it.hasNext();) {
            SessionNpcListener l = (SessionNpcListener) it.next();
            l.onForcedHeadChat(n, message);
        }
    }

    public void fireFaceTile(NPC n, int x, int y) {
        for (Iterator it = this.getListeners().iterator(); it.hasNext();) {
            SessionNpcListener l = (SessionNpcListener) it.next();
            l.onFaceTile(n, x, y);
        }
    }

    public void fireHeathChanged(NPC n, int current, int max) {
        for (Iterator it = this.getListeners().iterator(); it.hasNext();) {
            SessionNpcListener l = (SessionNpcListener) it.next();
            l.onHealthChange(n, current, max);
        }
    }
}
