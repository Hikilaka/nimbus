/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.clientobservers;

import bot.listener.ChatListener;
import bot.model.ChatMessage;
import bot.slave.hook.Client;
import java.util.Iterator;
import java.util.Observable;

/**
 *
 * @author Silabsoft
 */
public class ChatObserver extends ClientObserver {

    private String name;
    private String message;
    private int type;

    public ChatObserver() {
    }

    @Override
    public void update(Observable o, Object o1) {
        Client c = (Client) o1;
        String[] m = c.getChatMessages();
        String[] n = c.getChatNames();
        int[] t = c.getChatTypes();

        if (m[0] != null) {
            int off = getChatOffset(t, n, m);

            for (int i = off; i > 0; i--) {

                this.fireChatMessageAdded(new ChatMessage(t[i], n[i], m[i]));
            }
            message = m[0];
            type = t[0];
            name = n[0];
        }
    }

    public void fireChatMessageAdded(ChatMessage c) {
        for (Iterator it = this.getListeners().iterator(); it.hasNext();) {
            ChatListener l = (ChatListener) it.next();
            l.onChatMessage(c);
        }
    }

    private int getChatOffset(int[] t, String[] n, String[] m) {
        if (message == null && name == null) {
            return -1;
        }
        for (int i = 0; i < 20; i++) {
            if (m[i] == null) {
                continue;
            }
            if (t[i] == type && m[i].equals(message)) {
                if (t[i] != 0 && n[i].equals(name)) {

                    return i;
                }
                if (t[i] == 0) {

                    return i;
                }
            }
        }
        return -1;
    }
}
