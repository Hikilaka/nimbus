/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.clientobservers;

import bot.listener.BotEventListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Silabsoft
 */
public class ClientObserver implements Observer{
    
    private final ArrayList<BotEventListener> listeners;
    @Override
    public void update(Observable o, Object o1) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ClientObserver() {
        this.listeners = new ArrayList<BotEventListener>();
    }
    public void addListener(BotEventListener l){
        this.listeners.add(l);
    }
    public void removeListener(BotEventListener l){
        this.listeners.remove(l);
    }
    public List getListeners(){
        return ((ArrayList) listeners.clone());
    }
}
