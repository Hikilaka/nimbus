/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot;

/**
 *
 * @author Silabsoft
 */
import java.math.BigInteger;
import org.apache.bcel.generic.ArrayType;
import org.apache.bcel.generic.ObjectType;
import org.apache.bcel.generic.Type;

public class Constant {

    public static final String JAR_OUT_NAME = "nimbus.jar";
    public static final String MASTER_NAME = "MASTER";
    public static final String PACKAGE_NAME = "bot";
    public static final String HOOK_PACKAGE = PACKAGE_NAME + ".slave.hook";
    public static final String JAR_IN_NAME = "client.jar";
    public static final String EVENT_FOLDER = "Events";
    // Types
    public static final ArrayType byteArrayType = new ArrayType(Type.BYTE, 1);
    public static final ArrayType integerArrayType = new ArrayType(Type.INT, 1);
    public static final ArrayType stringArrayType = new ArrayType(Type.STRING, 1);
    public static final ObjectType bigIntegerType = new ObjectType(BigInteger.class.getName());
    
    
    public static final int SKILL_COUNT = 21;
}