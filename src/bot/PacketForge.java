package bot;

import bot.slave.hook.Client;
import bot.slave.hook.Stream;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author Silabsoft
 */
public class PacketForge {

    public static final int ACTION_THREE_NPC_HEADER = 21;
    public static final int ATTACK_NPC_HEADER = 72;
    public static final int ACTION_ONE_NPC_HEADER = 155;
    public static final int ACTION_TWO_NPC_HEADER = 17;
    public static final int PICKUP_ITEM_HEADER = 236;
    private final Stream stream;

    public PacketForge(Stream stream) {
        this.stream = stream;
    }

    public void attackNpc(int index) {
        npcAction(index, 0);
    }

    public void pickupItem(int id, int baseX, int baseY, int itemBaseX, int itemBaseY) {
        stream.p1isaac(PICKUP_ITEM_HEADER);
        stream.ip2(itemBaseY + baseY);
        stream.p2(id);
        stream.ip2(itemBaseX + baseX);
    }

    public void useItem(int frame, int slot, int id) {
        stream.p1isaac(122);
        stream.isp2(frame);
        stream.sp2(slot);
        stream.ip2(id);
    }

    public void npcAction(int index, int actionId) {
        switch (actionId) {
            case 0:
                stream.p1isaac(ATTACK_NPC_HEADER);
                stream.sp2(index);
                break;
            case 1:
                stream.p1isaac(ACTION_ONE_NPC_HEADER);
                stream.ip2(index);
                break;
            case 2:
                stream.p1isaac(ACTION_TWO_NPC_HEADER);
                stream.isp2(index);
                break;
            case 3:
                stream.p1isaac(ACTION_THREE_NPC_HEADER);
                stream.p2(index);
            default:
                Bot.log("INVALID NPC ACTION ID");
        }
    }
}
